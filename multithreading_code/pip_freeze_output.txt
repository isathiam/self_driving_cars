absl-py==0.15.0
aiohttp @ file:///D:/bld/aiohttp_1603560595546/work
appdirs==1.4.4
argon2-cffi @ file:///C:/ci/argon2-cffi_1613037869401/work
astor @ file:///home/conda/feedstock_root/build_artifacts/astor_1593610464257/work
astunparse @ file:///home/conda/feedstock_root/build_artifacts/astunparse_1610696312422/work
async-generator @ file:///home/ktietz/src/ci/async_generator_1611927993394/work
async-timeout==3.0.1
attrs @ file:///opt/conda/conda-bld/attrs_1642510447205/work
audioread==2.1.9
backcall @ file:///home/ktietz/src/ci/backcall_1611930011877/work
bleach @ file:///opt/conda/conda-bld/bleach_1641577558959/work
blinker==1.4
brotlipy==0.7.0
cached-property @ file:///home/conda/feedstock_root/build_artifacts/cached_property_1615209429212/work
cachetools @ file:///home/conda/feedstock_root/build_artifacts/cachetools_1633010882559/work
certifi==2021.10.8
cffi @ file:///C:/ci/cffi_1636523946399/work
chardet @ file:///D:/bld/chardet_1602255509283/work
charset-normalizer @ file:///tmp/build/80754af9/charset-normalizer_1630003229654/work
clang==5.0
click @ file:///D:/bld/click_1635822757640/work
colorama @ file:///tmp/build/80754af9/colorama_1607707115595/work
conda==4.11.0
conda-package-handling @ file:///C:/ci/conda-package-handling_1618262410900/work
cryptography @ file:///C:/ci/cryptography_1639414720302/work
cycler @ file:///tmp/build/80754af9/cycler_1637851556182/work
Cython==0.29.27
dataclasses @ file:///home/conda/feedstock_root/build_artifacts/dataclasses_1628958434797/work
debugpy @ file:///C:/ci/debugpy_1637091961445/work
decorator @ file:///tmp/build/80754af9/decorator_1632776554403/work
defusedxml @ file:///tmp/build/80754af9/defusedxml_1615228127516/work
dill==0.3.4
dm-tree==0.1.6
entrypoints==0.3
fire==0.4.0
flatbuffers @ file:///home/conda/feedstock_root/build_artifacts/python-flatbuffers_1617723079010/work
fonttools==4.25.0
future==0.18.2
gast @ file:///home/conda/feedstock_root/build_artifacts/gast_1596839682936/work
gin-config==0.5.0
google-api-core==2.5.0
google-api-python-client==2.37.0
google-auth @ file:///home/conda/feedstock_root/build_artifacts/google-auth_1629296548061/work
google-auth-httplib2==0.1.0
google-auth-oauthlib @ file:///home/conda/feedstock_root/build_artifacts/google-auth-oauthlib_1630497468950/work
google-cloud-bigquery==2.32.0
google-cloud-core==2.2.2
google-crc32c==1.3.0
google-pasta==0.2.0
google-resumable-media==2.2.1
googleapis-common-protos==1.54.0
grpcio @ file:///D:/bld/grpcio_1639715948696/work
grpcio-status==1.43.0
gurobipy==9.5.0
h5py @ file:///D:/bld/h5py_1637964127763/work
httplib2==0.20.4
idna @ file:///tmp/build/80754af9/idna_1637925883363/work
importlib-metadata @ file:///C:/ci/importlib-metadata_1638543082309/work
ipykernel @ file:///C:/ci/ipykernel_1633545585502/work/dist/ipykernel-6.4.1-py3-none-any.whl
ipython @ file:///C:/ci/ipython_1635944283918/work
ipython-genutils @ file:///tmp/build/80754af9/ipython_genutils_1606773439826/work
ipywidgets @ file:///tmp/build/80754af9/ipywidgets_1634143127070/work
jedi @ file:///C:/ci/jedi_1611341083684/work
Jinja2 @ file:///tmp/build/80754af9/jinja2_1635780242639/work
joblib==1.1.0
jsonschema @ file:///Users/ktietz/demo/mc3/conda-bld/jsonschema_1630511932244/work
jupyter @ file:///C:/ci/jupyter_1607685287094/work
jupyter-client @ file:///tmp/build/80754af9/jupyter_client_1640335223713/work
jupyter-console @ file:///tmp/build/80754af9/jupyter_console_1616615302928/work
jupyter-core @ file:///C:/ci/jupyter_core_1636537172022/work
jupyterlab-pygments @ file:///tmp/build/80754af9/jupyterlab_pygments_1601490720602/work
jupyterlab-widgets @ file:///tmp/build/80754af9/jupyterlab_widgets_1609884341231/work
kaggle==1.5.12
keras==2.8.0
Keras-Preprocessing @ file:///home/conda/feedstock_root/build_artifacts/keras-preprocessing_1610713559828/work
kiwisolver @ file:///C:/ci/kiwisolver_1612282555033/work
labelImg @ file:///D:/bld/labelimg_1636761465427/work
labeling==0.1.5
libmambapy @ file:///D:/bld/mamba-split_1638990129268/work/libmambapy
librosa==0.8.1
llvmlite==0.36.0
lxml @ file:///D:/bld/lxml_1639527290187/work
mamba @ file:///D:/bld/mamba-split_1638990129268/work/mamba
Markdown @ file:///home/conda/feedstock_root/build_artifacts/markdown_1637220118004/work
MarkupSafe @ file:///C:/ci/markupsafe_1621528502553/work
matplotlib @ file:///C:/ci/matplotlib-suite_1634667159685/work
matplotlib-inline @ file:///tmp/build/80754af9/matplotlib-inline_1628242447089/work
menuinst==1.4.16
mistune @ file:///C:/ci/mistune_1607359457024/work
mkl-fft==1.3.1
mkl-random @ file:///C:/ci/mkl_random_1626186184308/work
mkl-service==2.4.0
MouseInfo==0.1.3
multidict @ file:///D:/bld/multidict_1642968629384/work
munkres==1.1.4
nbclient @ file:///tmp/build/80754af9/nbclient_1614364831625/work
nbconvert @ file:///C:/ci/nbconvert_1624479160025/work
nbformat @ file:///tmp/build/80754af9/nbformat_1617383369282/work
nest-asyncio @ file:///tmp/build/80754af9/nest-asyncio_1613680548246/work
neural-structured-learning==1.3.1
notebook @ file:///C:/ci/notebook_1637161941035/work
numba==0.53.0
numpy==1.20.3
oauthlib @ file:///home/conda/feedstock_root/build_artifacts/oauthlib_1622563202229/work
olefile @ file:///Users/ktietz/demo/mc3/conda-bld/olefile_1629805411829/work
onnx==1.10.2
opencv-contrib-python==4.4.0.46
opencv-contrib-python-headless==4.4.0.46
opencv-python==4.4.0.46
opencv-python-headless==4.5.5.62
opt-einsum @ file:///home/conda/feedstock_root/build_artifacts/opt_einsum_1617859230218/work
packaging @ file:///tmp/build/80754af9/packaging_1637314298585/work
pandas==1.4.0
pandocfilters @ file:///C:/ci/pandocfilters_1605114832805/work
parso @ file:///opt/conda/conda-bld/parso_1641458642106/work
pickleshare @ file:///tmp/build/80754af9/pickleshare_1606932040724/work
Pillow==8.2.0
pooch==1.6.0
prometheus-client @ file:///tmp/build/80754af9/prometheus_client_1637050397234/work
promise==2.3
prompt-toolkit @ file:///tmp/build/80754af9/prompt-toolkit_1633440160888/work
proto-plus==1.20.0
protobuf==3.19.4
psutil==5.9.0
py-cpuinfo==8.0.0
pyasn1==0.4.8
pyasn1-modules==0.2.7
PyAutoGUI==0.9.53
pybind11==2.9.1
pycosat==0.6.3
pycparser @ file:///tmp/build/80754af9/pycparser_1636541352034/work
PyGetWindow==0.0.9
Pygments @ file:///tmp/build/80754af9/pygments_1629234116488/work
PyJWT @ file:///home/conda/feedstock_root/build_artifacts/pyjwt_1638819640841/work
pymongo==3.11.0
PyMsgBox==1.0.9
pynput==1.7.4
pyOpenSSL @ file:///tmp/build/80754af9/pyopenssl_1635333100036/work
pyparsing @ file:///tmp/build/80754af9/pyparsing_1635766073266/work
pyperclip==1.8.2
PyQt5==5.12.3
PyQt5_sip==4.19.18
PyQtChart==5.12
PyQtWebEngine==5.12.1
PyRect==0.1.4
pyrsistent @ file:///C:/ci/pyrsistent_1636093225342/work
PyScreeze==0.1.28
PySocks @ file:///C:/ci/pysocks_1605307512533/work
python-dateutil @ file:///tmp/build/80754af9/python-dateutil_1626374649649/work
python-slugify==5.0.2
pytweening==1.0.4
pytz==2021.3
pyu2f @ file:///home/conda/feedstock_root/build_artifacts/pyu2f_1604248910016/work
pywin32==302
pywinpty @ file:///C:/ci/pywinpty_1607419945780/work
PyYAML==6.0
pyzmq @ file:///C:/ci/pyzmq_1638435148211/work
qrcode==7.3.1
qtconsole @ file:///tmp/build/80754af9/qtconsole_1632739723211/work
QtPy @ file:///tmp/build/80754af9/qtpy_1629397026935/work
requests @ file:///opt/conda/conda-bld/requests_1641824580448/work
requests-oauthlib @ file:///home/conda/feedstock_root/build_artifacts/requests-oauthlib_1595492159598/work
resampy==0.2.2
rsa @ file:///home/conda/feedstock_root/build_artifacts/rsa_1637781155505/work
ruamel-yaml-conda @ file:///C:/ci/ruamel_yaml_1616016898638/work
scikit-learn==1.0.2
scipy @ file:///C:/ci/scipy_1641555170412/work
Send2Trash @ file:///tmp/build/80754af9/send2trash_1632406701022/work
sentencepiece==0.1.96
sip==4.19.13
six @ file:///tmp/build/80754af9/six_1623709665295/work
SoundFile==0.10.3.post1
tensorboard @ file:///home/conda/feedstock_root/build_artifacts/tensorboard_1629677129676/work/tensorboard-2.6.0-py3-none-any.whl
tensorboard-data-server @ file:///D:/bld/tensorboard-data-server_1636046032268/work/tensorboard_data_server-0.6.0-py3-none-any.whl
tensorboard-plugin-wit @ file:///home/conda/feedstock_root/build_artifacts/tensorboard-plugin-wit_1641458951060/work/tensorboard_plugin_wit-1.8.1-py3-none-any.whl
tensorflow==2.6.0
tensorflow-addons==0.15.0
tensorflow-datasets==4.5.2
tensorflow-estimator @ file:///home/builder/adipietro/tf/tensorflow-estimator_1630508970172/work/tensorflow_estimator-2.6.0-py2.py3-none-any.whl
tensorflow-hub==0.12.0
tensorflow-metadata==1.6.0
tensorflow-model-optimization==0.7.1
tensorflowjs==3.13.0
termcolor==1.1.0
terminado==0.9.4
testpath @ file:///tmp/build/80754af9/testpath_1624638946665/work
text-unidecode==1.3
tf-models-official==2.3.0
tf-slim==1.1.0
tflite==2.4.0
tflite-model-maker==0.3.4
tflite-support==0.3.1
tflite2onnx==0.4.0
threadpoolctl==3.1.0
tifffile==2021.4.8
torch==1.10.2+cu102
torchaudio==0.10.2+cu102
torchvision==0.11.3+cu102
tornado @ file:///C:/ci/tornado_1606924294691/work
tqdm @ file:///tmp/build/80754af9/tqdm_1635330843403/work
traitlets @ file:///tmp/build/80754af9/traitlets_1636710298902/work
typeguard==2.13.3
typing-extensions @ file:///tmp/build/80754af9/typing_extensions_1631814937681/work
uritemplate==4.1.1
urllib3==1.25.11
wcwidth @ file:///Users/ktietz/demo/mc3/conda-bld/wcwidth_1629357192024/work
webencodings==0.5.1
Werkzeug @ file:///home/conda/feedstock_root/build_artifacts/werkzeug_1642714909499/work
widgetsnbextension @ file:///C:/ci/widgetsnbextension_1607531582688/work
win-inet-pton @ file:///C:/ci/win_inet_pton_1605306162074/work
wincertstore==0.2
wrapt @ file:///D:/bld/wrapt_1635836536035/work
yarl @ file:///D:/bld/yarl_1636047033860/work
zipp @ file:///opt/conda/conda-bld/zipp_1641824620731/work
