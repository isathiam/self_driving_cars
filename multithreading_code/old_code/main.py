import threading
import cv2
import pathlib
import time
import os
import lane_detection as ld
import numpy as np
import copy
from functools import partial

import caffe2.python.onnx.backend as backend
import torch
#import onnx
print(f"CUDA availiable: {torch.cuda.is_available()}, Card Detected: {torch.cuda.get_device_name(0)}")

# WARNING: This import HAS to be below the torch import
from detector import ObjectDetectorOptions, ObjectDetector


OUT_PATH                 = "output"
DETECTION_THRESHOLD      = 0.5
ONNX_MODEL_PATH          = "obj_det_lite2.onnx"
TFLITE_MODEL_PATH        = "object_detection_lite2.tflite" 
TRACKER                  = cv2.TrackerKCF_create()

""" Shared Resources """
current_frame   = None  # Image of the current "real-time" frame
current_bboxes  = []    # Current bounding boxes outputted by the object detector
current_bnames  = []    # Current bounding box labels outputted by the object detector
tracking_bboxes = []    # Current bounding boxes outputted by the tracker
lane_output     = None  # Partial function that draws the current lane detection on the given frame


def drawRectangle(frame, bbox):
    """ Draws a rectangle on the given frame """
    p1 = (int(bbox[0]), int(bbox[1]))
    p2 = (int(bbox[0] + bbox[2]), int(bbox[1] + bbox[3]))
    cv2.rectangle(frame, p1, p2, (255, 0, 0), 2, 1)

    
def drawText(frame, txt, location, color = (0, 0, 0)):
    """ Puts text on the given frame """
    cv2.putText(frame, txt, location, cv2.FONT_HERSHEY_PLAIN, 2, color, 1)


def main(video_path):
    """"
    Main function that starts all of the threads

    Arguments:
        video_path (str) : path to video to simulate
    """

    global current_frame
    
    # Load the video to simulate
    video = cv2.VideoCapture(video_path)
    _, current_frame = video.read()

    width  = int(video.get(cv2.CAP_PROP_FRAME_WIDTH))
    height = int(video.get(cv2.CAP_PROP_FRAME_HEIGHT))

    # Event objects that signal the threads when new data is available
    obj_detection_event  = threading.Event()
    new_frame_event      = threading.Event()
    new_bboxes_event     = threading.Event()
    lane_detection_event = threading.Event()
    obj_tracking_event   = threading.Event()

    # Mutex locks for the shared resources
    bbox_lock            = threading.Lock()
    frame_lock           = threading.Lock()
    tracking_lock        = threading.Lock()
    lane_output_lock     = threading.Lock()

    # Basename of the output video file
    name = pathlib.Path(video_path).stem

    # Threads for object detection, tracking, and lane detection 
    obj_detection_t  = threading.Thread(target = thread_object_detection, args = (obj_detection_event, new_bboxes_event, bbox_lock, frame_lock, width, height))
    tracker_t        = threading.Thread(target = thread_tracker,          args = (obj_tracking_event, new_bboxes_event, bbox_lock, frame_lock, tracking_lock))
    lane_detection_t = threading.Thread(target = thread_lane_detection,   args = (lane_detection_event, frame_lock, lane_output_lock))
    video_output_t   = threading.Thread(target = thread_video_output,     args = (new_frame_event, frame_lock, tracking_lock, lane_output_lock, name, width, height))

    # Start all of the threads
    obj_detection_t.start()
    obj_detection_event.set()

    # Give the object detector a second to run
    time.sleep(1)

    tracker_t.start()
    lane_detection_t.start()
    video_output_t.start()

    frame_count = 1

    # Loop over each frame of the video, waiting 1/30th of a second 
    # between frames to simulate it as real incoming video data
    while True:
        ok, frame = video.read()
        if ok: 
            # Update the global current frame and signal to threads a new frame is available
            frame_lock.acquire()
            current_frame = frame
            frame_lock.release()

            # Run output video on every frame
            new_frame_event.set()

            # Run lane detection every frame
            lane_detection_event.set()
          
            # Run the object detection on every frame possible
            obj_detection_event.set()

            # Run object tracking on every frame
            obj_tracking_event.set()

            print(f"Frame: {frame_count}")
            frame_count += 1

            time.sleep(1 / 5)
            
        else:
            # If reached the end of the video, join all threads and save output video
            break

    obj_detection_t.join()
    tracker_t.join()
    lane_detection_t.join()


def thread_video_output(new_frame_event, frame_lock, tracking_lock, lane_output_lock, name, width, height):
    """
    Writes the most up-to-date bounding boxes and lane detection views on each frame

    Arguments:
        new_frame_event  (Event) : Signals the thread that a new frame is available
        frame_lock       (mutex) : Lock for accessing the current frame   
        tracking_lock    (mutex) : Lock for accessing the tracking bounding boxes
        lane_output_lock (mutex) : Lock for accessing the current lane output
        name             (str)   : Basename of output video file
        height           (int)   : Height of the video (in pixels)
        width            (int)   : Width of the video (in pixels)
    """

    global tracking_bboxes
    global current_bnames
    global lane_output

    video_out = cv2.VideoWriter(f"{OUT_PATH}/{name}_tracking_lane_combined.mp4", cv2.VideoWriter_fourcc(*'DIVX'), 10, (width, height))

    while new_frame_event.wait(1):
        new_frame_event.clear()
        
        # Get a copy of the current frame
        frame_lock.acquire()
        frame_cpy = np.copy(current_frame)
        frame_lock.release()

        # Draw the most up-to-date bounding box outputted by the tracker
        for i in range(len(tracking_bboxes)):
            try:
                drawRectangle(frame_cpy, tracking_bboxes[i])
                drawText(frame_cpy, current_bnames[i], (int(tracking_bboxes[i][0]), int(tracking_bboxes[i][1] - 10)), (0, 0, 0))
            except IndexError: # Since we aren't using a lock here this can happen
                pass
        # Draw the most up-to-date lane detection
        lane_func = lane_output
        if lane_func is not None:
            frame_cpy = lane_func(frame_cpy)

        # Save frame to output video
        video_out.write(frame_cpy)
        print("Saved frame to output")
    
    video_out.release()


def thread_object_detection(obj_detection_event, new_bboxes_event, bbox_lock, frame_lock, width, height):
    """
    Runs object detection as fast as possible on the current frame

    Arguments:
        obj_detection_event (Event) : Signals the thread to run object detection on the current frame
        new_bboxes_event    (Event) : Signals the thread that new bounding boxes are available from detector
        bbox_lock           (mutex) : Lock for accessing the outputted detector bounding boxes
        frame_lock          (mutex) : Lock for accessing the current frame   
        height              (int)   : Height of the video (in pixels)
        width               (int)   : Width of the video (in pixels)
    """
    global current_bnames
    global current_bboxes
    
    # Load the TFLite model
    options = ObjectDetectorOptions(num_threads = 8, score_threshold = DETECTION_THRESHOLD)
    detector = ObjectDetector(model_path = TFLITE_MODEL_PATH, options = options)
    
    while obj_detection_event.wait(2):
        obj_detection_event.clear()
        
        frame_lock.acquire()
        frame_cpy = np.copy(current_frame)
        frame_lock.release()

        new_current_bboxes, new_current_bnames = [], []
        for detection in detector.detect(frame_cpy):
            box = (max(detection.bounding_box.left, 0),
                   max(detection.bounding_box.top, 0),
                   min(width - 1, detection.bounding_box.right) - max(detection.bounding_box.left, 0),
                   min(height - 1, detection.bounding_box.bottom) - max(detection.bounding_box.top, 0))
            new_current_bboxes.append(box)
            new_current_bnames.append(f"{detection.categories[0].label} ({str(detection.categories[0].score)[:3]})")
        
        bbox_lock.acquire()
        current_bboxes = new_current_bboxes
        current_bnames = new_current_bnames
        bbox_lock.release()
        
        new_bboxes_event.set()

        print("Object Detection: new bounding boxes available")


def thread_tracker(obj_tracking_event, new_bboxes_event, bbox_lock, frame_lock, tracking_lock):
    """
    Runs object tracking on each from (or as fast as it can)

    Arguments:
        obj_tracking_event (Event) : Signals the thread that a new frame is available
        new_bboxes_event   (Event) : Signals the thread that new bounding boxes are available from detector
        bbox_lock          (mutex) : Lock for accessing the outputted detector bounding boxes
        frame_lock         (mutex) : Lock for accessing the current frame   
        tracking_lock      (mutex) : Lock for accessing the tracking boudning boxes
    """
    global current_bboxes
    global current_bnames
    global tracking_bboxes

    trackers = []
    
    # loop over incomming frames
    while obj_tracking_event.wait(1):
        obj_tracking_event.clear()
        
        # Make a copy of the current frame
        frame_lock.acquire()
        frame_cpy = np.copy(current_frame)
        frame_lock.release()

        # If there are new bounding boxes then redo all trackers with the new boxes
        if new_bboxes_event.wait(0):
            new_bboxes_event.clear()
            trackers = []

            # Copy the current bounding boxes outputted by the object detector
            bbox_lock.acquire()
            bboxes = copy.copy(current_bboxes)
            bbox_lock.release()

            # Copy the same bounding boxes as the tracking bounding boxes
            # and initialize the trackers on them
            tracking_lock.acquire()
            tracking_bboxes = bboxes
            for i in range(len(tracking_bboxes)):
                tracker =  cv2.TrackerKCF_create()
                tracker.init(frame_cpy, tracking_bboxes[i])
                trackers.append(tracker)
            tracking_lock.release()

            print("Object Tracking: recieved updated bounding boxes")

        # Each new frame run the tracking on the bounding boxes
        for i in range(len(trackers)):
            _, bbox = trackers[i].update(frame_cpy)
            tracking_lock.acquire()
            tracking_bboxes[i] = bbox
            tracking_lock.release()
        
        print("Object Tracking: new tracking boxes available")


def thread_lane_detection(lane_detection_event, frame_lock, lane_output_lock):
    """
    Runs lane detection on the most current frame
    
    Arguments:
        lane_detection_event (Event) : Signals the thread to run lane detection on new frame
        frame_lock           (mutex) : Lock for accessing the current frame          
        lane_output_lock     (mutex) : Lock for accessing the lane output function 
    """

    global lane_output
    # TODO: Document what each of these steps are
    while lane_detection_event.wait(1):
        lane_detection_event.clear()

        frame_lock.acquire()
        frame_cpy = np.copy(current_frame)
        frame_lock.release()

        ksize = 15
        binary_output = ld.binary_image(frame_cpy, sobel_kernel = ksize, thresh = (30, 100))

        binary_warped = ld.warp_image(binary_output)

        ploty, left_fitx, right_fitx, out_img = ld.fit_polynomial(binary_warped)

        draw_output = ld.draw_detection(ploty, left_fitx, right_fitx, out_img, binary_warped)

        unwarped = ld.unwarp_image(draw_output)
        
        # left_curverad, right_curverad = ld.measure_curvature_real(binary_warped)
        # offset = ld.center_offset(binary_warped)

        lane_output_lock.acquire()
        lane_output = partial(cv2.addWeighted, alpha = 1, src2 = unwarped, beta = 0.3, gamma = 0)
        lane_output_lock.release()

        print("Lane Detection: new lane detection avaialble")


if __name__ == "__main__":
    if not os.path.exists(OUT_PATH):
        os.mkdir(OUT_PATH)

    for video_path in pathlib.Path("recordings").iterdir():
        print(f"Running simulation on {str(video_path)}")
        main(str(video_path))