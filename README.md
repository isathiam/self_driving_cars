# Autonomous Driving Framework
A framework for autonomous driving, currently able to detect and track cars, vans, trucks, people, and lanes.
Contact antholn1@uci.edu, tranck1@uci.edu, or isathiam@uci.edu for permissions to the Google Drive.

## Capstone Branches
* Real-Life: test and improve the current framework on real-life data
* Non-Controlling Simulation: Continue working in Traffic-3D to improve detecteable objects and more complex scenarios
* Controlling Simulation: Take control of the car using a framework such as CARLA to control a car in a virtual enviornment
* Anything else you can think of

## To-Dos
* Output and use lane drift (already implemented, see comments)
* Attempt to output object distance (would need for controlling)